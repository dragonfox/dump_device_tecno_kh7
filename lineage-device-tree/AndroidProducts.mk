#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TECNO-KH7.mk

COMMON_LUNCH_CHOICES := \
    lineage_TECNO-KH7-user \
    lineage_TECNO-KH7-userdebug \
    lineage_TECNO-KH7-eng
